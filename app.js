const http = require('http');
const url = require('url');
const fs = require('fs');

const port = 8080;
const hostname = 'localhost';

const server = http.createServer((req, res) => {
	res.statusCode = 200;
	res.setHeader = ('Content-type', 'text/html');
	
	const filename = req.url.substring(1);
	console.log("Req filename: " + filename);
	
	fs.readFile(filename, (err, html) => {
		res.write(html);
	});
});

server.listen(port, hostname, () => {
	console.log('Server running listening on port: ' + port);	
});
